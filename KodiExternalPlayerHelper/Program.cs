﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Microsoft.Data.Sqlite;
using System.Diagnostics;
using System.Data;
using NLog;
using NLog.Config;
using NLog.Targets;
using System.Threading;
using System.Windows.Forms;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Threading.Tasks;


namespace KodiExternalPlayerHelper
{
    class Program : ApplicationContext
    {
            
        static bool Visible = false;
        static string mediaDuration = "";
        static TimeSpan mediaTimeSpan = new TimeSpan(0, 0, 0);
        static string currentPlayingMedia = "";
        static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        static NotifyIcon tray = new NotifyIcon();
        static ContextMenuStrip contextMenu = new ContextMenuStrip();
        static Int64 idFile = 0;
        static double elapsedSeconds = 0;
        static double mediaTimeInSeconds = 0;
        static double bookMarkStartTime = 0;
        static string latestKodiDatabase = GetLatestKodiDatabase() ?? "";
        
        //Create a new thread to avoid blocking the main thread with a while loop 
        static Thread tMediaStopwatch = new Thread(MediaStopwatch);

        static void Main(string[] args)
        {
            Application.Run(new Program(args));
        }

        public Program(string[] args)
        {
            Console.Title = "Kodi External Player Helper";
            Thread.Sleep(300);
            SetConsoleWindowVisibility(Visible);

            //Console.SetWindowSize((Console.WindowWidth*2),(Console.WindowHeight*2));           

            #region Kodi ascii Logo


            string kodiAsciiLogo = Environment.NewLine + Environment.NewLine +
            "                             ..::::..                          " + Environment.NewLine +
            "                           .:::-----::.                        " + Environment.NewLine +
            "                         .::----------::.                      " + Environment.NewLine +
            "                       .::--------------::.                    " + Environment.NewLine +
            "                     .::------------------::.                  " + Environment.NewLine +
            "                    ::----------------------::                 " + Environment.NewLine +
            "                    -----------------------:.                  " + Environment.NewLine +
            "                    ---------------------:.       .            " + Environment.NewLine +
            "             .      -------------------:.       :=::.          " + Environment.NewLine +
            "           .::      -----------------:.       :=----::.        " + Environment.NewLine +
            "         .::--.     ---------------:.       :=--------::.      " + Environment.NewLine +
            "       .::----.     -------------:.       :=------------::.    " + Environment.NewLine +
            "      ::------.     -----------:.       :=----------------::   " + Environment.NewLine +
            "     :--------.     ---------:.       :=--------------------:  " + Environment.NewLine +
            "     ---------.     -------:.        .:----------------------  " + Environment.NewLine +
            "     :--------.     -----:.             :-------------------:  " + Environment.NewLine +
            "      .:------.     ---:.       :-        :---------------:.   " + Environment.NewLine +
            "        .:----.     -:.       :=--=-        :-----------:.     " + Environment.NewLine +
            "          .:--.     .       :=------=-        :-------:.       " + Environment.NewLine +
            "            .:.           :=----------=-       .:---:.         " + Environment.NewLine +
            "              :.        :=--------------=-       .:.           " + Environment.NewLine +
            "               .-.    :=------------------=-    .:.            " + Environment.NewLine +
            "                 .-.:=----------------------=-.-.              " + Environment.NewLine +
            "                   .::----------------------::.                " + Environment.NewLine +
            "                      .:------------------::.                  " + Environment.NewLine +
            "                        .:--------------::.                    " + Environment.NewLine +
            "                          .:----------::.                      " + Environment.NewLine +
            "                            .::----:::.                        " + Environment.NewLine +
            "                              ..::..                           " + Environment.NewLine + Environment.NewLine + Environment.NewLine + Environment.NewLine +
            "                   /////////////////////////";



            string kodiAsciiText = Environment.NewLine + Environment.NewLine + Environment.NewLine +

            "                   88  dP  dP\"Yb  8888b.  88" + Environment.NewLine +
            "                   88odP  dP   Yb  8I  Yb 88" + Environment.NewLine +
            "                   88\"Yb  Yb   dP  8I  dY 88" + Environment.NewLine +
            "                   88  Yb  YbodP  8888Y\"  88" + Environment.NewLine +
            Environment.NewLine +
            Environment.NewLine +

            "        888888 Yb  dP 888888 888888 88\"\"Yb 88b 88    db    88         " + Environment.NewLine +
            "        88__    YbdP    88   88__   88__dP 88Yb88   dPYb   88         " + Environment.NewLine +
            "        88\"\"    dPYb    88   88\"\"   88\"Yb  88 Y88  dP__Yb  88  .o     " + Environment.NewLine +
            "        888888 dP  Yb   88   888888 88  Yb 88  Y8 dP\"\"\"\"Yb 88ood8     " + Environment.NewLine +
            "                                                                                     " + Environment.NewLine +
            "                                                                                     " + Environment.NewLine +
            "            88\"\"Yb 88        db    Yb  dP 888888 88\"\"Yb                   " + Environment.NewLine +
            "            88__dP 88       dPYb    YbdP  88__   88__dP                   " + Environment.NewLine +
            "            88\"\"\"  88  .o  dP__Yb    8P   88\"\"   88\"Yb                    " + Environment.NewLine +
            "            88     88ood8 dP\"\"\"\"Yb  dP    888888 88  Yb                   " + Environment.NewLine +
            "                                                                                     " + Environment.NewLine +
            "                                                                                     " + Environment.NewLine +
            "             88  88 888888 88     88\"\"Yb 888888 88\"\"Yb                     " + Environment.NewLine +
            "             88  88 88__   88     88__dP 88__   88__dP                     " + Environment.NewLine +
            "             888888 88\"\"   88  .o 88\"\"\"  88\"\"   88\"Yb                      " + Environment.NewLine +
            "             88  88 888888 88ood8 88     888888 88  Yb                     " + Environment.NewLine + Environment.NewLine + Environment.NewLine +
            "    /////////////////////////////////////////////////////////////" + Environment.NewLine + Environment.NewLine;

            Console.WriteLine(kodiAsciiLogo + kodiAsciiText);


            #endregion

            #region logging config

            string documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

            
            var config = new LoggingConfiguration();

            var consoleTarget = new ConsoleTarget
            {
                Name = "console",
                Layout = "${longdate}|${level:uppercase=true}|${logger}|${message}"
            };

            config.AddRule(LogLevel.Debug, LogLevel.Fatal, consoleTarget, "*");
            LogManager.Configuration = config;



            var fileTarget = new FileTarget
            {
                Name = "file",
                FileName = $"{documentsPath}\\KodiExternalPlayerHelper.txt",
                ArchiveFileName = "KodiExternalPlayerHelper.{#}.txt",
                ArchiveNumbering = NLog.Targets.ArchiveNumberingMode.Date,
                ArchiveEvery = NLog.Targets.FileArchivePeriod.Day,
                ArchiveDateFormat = "yyyyMMdd",
                Layout = "${longdate}|${level:uppercase=true}|${logger}|${message}"
            };

            config.AddRule(LogLevel.Debug, LogLevel.Fatal, fileTarget, "*");
            LogManager.Configuration = config;

            #endregion

            tray.Icon = Icon.ExtractAssociatedIcon(Application.ExecutablePath);
            tray.Visible = true;
            tray.Text = Application.ProductName;

            contextMenu.Items.Add("Exit", null, (s, e) => { tMediaStopwatch.Abort(); Application.Exit(); });
            contextMenu.Items.Add("Show console window", null, (s, e) => { Visible = true; SetConsoleWindowVisibility(Visible); });
            contextMenu.Items.Add("Hide console window", null, (s, e) => { Visible = false; SetConsoleWindowVisibility(Visible); });

            tray.ContextMenuStrip = contextMenu;
            contextMenu.BackColor = Color.FromArgb(15, 15, 15);
            contextMenu.ForeColor = Color.White;

            contextMenu.Items[0].Image = Resource.close;
            contextMenu.Items[1].Image = Resource.tray_open;
            contextMenu.Items[2].Image = Resource.tray;

            //contextMenu.Items[0].ForeColor = Color.Black;

            tray.DoubleClick += new EventHandler(tray_DoubleClick);
            tray.Click += new EventHandler(tray_Click);
            contextMenu.MouseLeave += new EventHandler(tray_MouseLeave);

            contextMenu.Items[0].MouseHover += new EventHandler(trayMenuItem0_MouseHover);
            contextMenu.Items[0].MouseLeave += new EventHandler(trayMenuItem0_MouseLeave);
            contextMenu.Items[1].MouseHover += new EventHandler(trayMenuItem1_MouseHover);
            contextMenu.Items[1].MouseLeave += new EventHandler(trayMenuItem1_MouseLeave);
            contextMenu.Items[2].MouseHover += new EventHandler(trayMenuItem2_MouseHover);
            contextMenu.Items[2].MouseLeave += new EventHandler(trayMenuItem2_MouseLeave);

            idFile = GetIdFile();

            StreamReader sr = new StreamReader("C:\\CurrentPlayingMedia.txt", Encoding.GetEncoding(1250));
            currentPlayingMedia = sr.ReadLine();
            logger.Info($"Got playing media from file \"CurrentPlayingMedia.txt\"... ({currentPlayingMedia}) ");

            Process mediainfo = new Process();
            mediainfo.StartInfo.WorkingDirectory = "C:\\Program Files\\MediaInfoCLI";
            mediainfo.StartInfo.FileName = "C:\\Program Files\\MediaInfoCLI\\mediainfo.exe";
            mediainfo.StartInfo.Arguments = $"{currentPlayingMedia} --Inform=\"Video;%Duration/String3%\"";
            mediainfo.StartInfo.UseShellExecute = false;
            mediainfo.StartInfo.RedirectStandardOutput = true;
            mediainfo.StartInfo.RedirectStandardError = true;
            mediainfo.StartInfo.CreateNoWindow = true;
            mediainfo.Start();
            mediainfo.WaitForExit();

            try
            {
                mediaDuration = mediainfo.StandardOutput.ReadLine();
                mediaDuration = mediaDuration.Substring(0, 8);
            }
            catch (Exception)
            {
                logger.Error($"could not get media timespan...onlinemedia? {Environment.NewLine} {Environment.NewLine}Path to media:{Environment.NewLine}{Environment.NewLine}{currentPlayingMedia}");
            }

            if (mediaDuration != "")
            {               
                tMediaStopwatch.Start();                
            }            
            
        }

        static Int64 GetIdFile() 
        {

            //get appdata path
            string appdataPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);

            //find the latest kodi database (the database with the highest number)
            string dataBasePath = $"{appdataPath}\\Kodi\\userdata\\Database\\";
            string[] files = Directory.GetFiles(dataBasePath, "MyVideos*.db", SearchOption.AllDirectories);
            string latestDataBase = FindStringWithHighestNumber(files);
            Int64 idFile = 0;
            int resultRowsCount = 0;

            // create connection
            string connectionString = $"Data Source={latestDataBase}";
            SqliteConnection connection = new SqliteConnection(connectionString);

            // open connection
            connection.Open();
            logger.Info("opening database connection...");

            // perform database operations

            // select data

            string selectQuery = "" +
            "SELECT DISTINCT" +
            " m.idFile," +
            " m.c22 as Filepath," +
            " f.playCount" +
            " FROM movie m" +
            " LEFT JOIN files f ON f.idFile = m.idFile" +
            $" WHERE Filepath like '%smb:{currentPlayingMedia.Replace("\\", "/").Replace("\"", "").TrimEnd(' ')}%'" +
            " UNION" +
            " SELECT DISTINCT" +
            " e.idFile," +
            " e.c18 as Filepath," +
            " f.playCount" +
            " FROM" +
            " episode e" +
            " LEFT JOIN files f ON f.idFile = e.idFile" +
            $" WHERE Filepath like '%smb:{currentPlayingMedia.Replace("\\", "/").Replace("\"", "").TrimEnd(' ')}%'";

            using (SqliteCommand command = new SqliteCommand(selectQuery, connection))
            {

                logger.Info("execute sql select query for getting fileid...");

                using (SqliteDataReader reader = command.ExecuteReader())
                {
                    DataTable dataTable = new DataTable();
                    dataTable.Load(reader);
                    if (dataTable.Rows.Count != 0)
                    {
                        idFile = dataTable.Rows[0].Field<Int64>("idFile");
                        resultRowsCount = dataTable.Rows.Count;
                        logger.Info($"select query result ok! (fileID: {idFile})");
                    }

                }
            }

            if (resultRowsCount == 0)
            {
                logger.Error($"Current playing media not found in kodi database! {Environment.NewLine}file: {currentPlayingMedia} ");
                Environment.Exit(0);
            }

            return idFile;
        }

        static string GetLatestKodiDatabase() 
        {
                        
            //get appdata path
            string appdataPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);

            //find the latest kodi database (the database with the highest number)
            string dataBasePath = $"{appdataPath}\\Kodi\\userdata\\Database\\";
            string[] files = Directory.GetFiles(dataBasePath, "MyVideos*.db", SearchOption.AllDirectories);
            string latestDataBase = FindStringWithHighestNumber(files);

            if (latestDataBase == "")
            {
                logger.Error("No kodi database found!");
            }

            return latestDataBase;
        
        }

        static bool GetBookMarkState() 
        {
           
            Int64 idFile = 0;
            int resultRowsCount = 0;
            bool bookmarkFound = false;

            // create connection
            string connectionString = $"Data Source={latestKodiDatabase}";
            SqliteConnection connection = new SqliteConnection(connectionString);

            // open connection
            connection.Open();
            logger.Info("opening database connection...");

            // perform database operations

            // select data

            string selectQuery = "" +
            $"SELECT TimeInSeconds FROM Bookmark WHERE idFile is {idFile}";

            using (SqliteCommand command = new SqliteCommand(selectQuery, connection))
            {

                logger.Info("execute sql select query for getting bookmark...");

                using (SqliteDataReader reader = command.ExecuteReader())
                {
                    DataTable dataTable = new DataTable();
                    dataTable.Load(reader);
                    if (dataTable.Rows.Count != 0)
                    {
                        bookMarkStartTime = dataTable.Rows[0].Field<double>("TimeInSeconds");
                        resultRowsCount = dataTable.Rows.Count; 
                        logger.Info($"Bookmark for idFile: {idFile} found!");
                        bookmarkFound = true;
                    }

                }
            }

            if (resultRowsCount == 0)
            {
                logger.Info($"No bookmark present for idFile: {idFile}");
                bookmarkFound = false;
            }

            return bookmarkFound;
        }            

        static void PersistWatchedStatus(Int64 idFile)
        {

           
            // create connection
            string connectionString = $"Data Source={latestKodiDatabase}";
            SqliteConnection connection = new SqliteConnection(connectionString);

            // open connection
            connection.Open();
            logger.Info("opening database connection...");


            // update data
            string updateQuery = $"UPDATE files set playcount=1 WHERE idFile={idFile}";
            using (SqliteCommand updateCommand = new SqliteCommand(updateQuery, connection))
            {
                logger.Info("updating watched status (executing sql update command)...");
                updateCommand.ExecuteNonQuery();
            }

            // close connection
            logger.Info("closing sql connection...");
            connection.Close();

        }

        static void PersistBookmark(Int64 idFile)
        {

            bool existsBookmark = false;
            
            // create connection
            string connectionString = $"Data Source={latestKodiDatabase}";
            SqliteConnection connection = new SqliteConnection(connectionString);

            // open connection
            connection.Open();
            logger.Info("opening database connection...");

            // new entry or update?

            string selectQuery = $"SELECT idFile FROM bookmark WHERE idFILE LIKE \"{idFile}\"";

            using (SqliteCommand command = new SqliteCommand(selectQuery, connection))
            {

                logger.Info("execute sql select query (Check if old bookmark exists...");

                try
                {
                    using (SqliteDataReader reader = command.ExecuteReader())
                    {
                        DataTable dataTable = new DataTable();
                        dataTable.Load(reader);
                        if (dataTable.Rows.Count != 0)
                        { existsBookmark = true; }
                    }
                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message);
                }
               
            }
                       
            // update data if bookmark was found

            if (existsBookmark)
            {
                string updateQuery = $"UPDATE bookmark set timeInSeconds='{elapsedSeconds}', TotalTimeInSeconds='{mediaTimeSpan.TotalSeconds}' WHERE idFile={idFile}";

                try
                {
                    using (SqliteCommand updateCommand = new SqliteCommand(updateQuery, connection))
                    {
                        logger.Info("Creating Bookmark (executing sql update command)...");
                        updateCommand.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message);
                }
             

                // close connection
                logger.Info("closing sql connection...");
                connection.Close();
                Environment.Exit(0);

            }

            // TODO: Try & Catch
            // create new bookmark if no old one was found

            if (!existsBookmark)
            {
                string insertQuery = $"INSERT INTO bookmark (idFIle,timeInSeconds,totalTimeInSeconds,thumbNailImage,player,playerState,\"type\") VALUES ({idFile},'{elapsedSeconds}','{mediaTimeSpan.TotalSeconds}',\"\",\"Videoplayer\",\"\",1)";

                try
                {
                    using (SqliteCommand updateCommand = new SqliteCommand(insertQuery, connection))
                    {
                        logger.Info("Creating Bookmark (executing sql insert command)...");
                        updateCommand.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message);
                }               

                // close connection
                logger.Info("closing sql connection...");
                connection.Close();
                Environment.Exit(0);
            }

        }

        static void MediaStopwatch() 
        {

            mediaTimeSpan = TimeSpan.Parse(mediaDuration);
            Stopwatch watch = new Stopwatch();

            while (Process.GetProcessesByName("mpc-be64").Length != 0)
            {
                    
                    watch.Start();

                    logger.Info("Stopwatch started...");

                    using (var progress = new ProgressBar())
                    {
                        progress.Report((double)watch.Elapsed.TotalSeconds / mediaTimeSpan.TotalSeconds);
                    }

                    while (watch.Elapsed.TotalSeconds < (mediaTimeSpan.TotalSeconds * 0.8))
                    {
                        string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}",
                        watch.Elapsed.Hours, watch.Elapsed.Minutes, watch.Elapsed.Seconds);
                        Console.Write($"Media playing: {elapsedTime} / {mediaTimeSpan}");
                        Thread.Sleep(1);
                        ClearCurrentConsoleLine();
                        if (Process.GetProcessesByName("mpc-be64").Length == 0) 
                        {
                            if (watch.Elapsed.TotalSeconds > (mediaTimeSpan.TotalSeconds * 0.1))
                            {
                                logger.Info("Mediaplaying aborted, setting bookmark...");
                                elapsedSeconds = watch.Elapsed.TotalSeconds;
                                PersistBookmark(idFile);
                                
                            }                            
                        }
                    }

                //if (Process.GetProcessesByName("mpc-be64").Length == 0) {break;}

                logger.Info($"End of time reached... current watched time: {watch.Elapsed} - " +
                                $"configured total seconds: {(mediaTimeSpan.TotalSeconds * 0.8)} watched seconds: {watch.Elapsed.Seconds}");
                    
                logger.Info("starting function \"PersistWatchedStatus...\"");
                    watch.Stop();

                if (mediaDuration != "")
                {
                    PersistWatchedStatus(idFile);
                    logger.Info($"watched status successful set for file: {Environment.NewLine}{currentPlayingMedia}");
                    Environment.Exit(1);
                }

                if (mediaDuration == "")
                {
                    Environment.Exit(0);
                }

               
            }                   

                elapsedSeconds = watch.Elapsed.TotalSeconds;
                mediaTimeInSeconds = mediaTimeSpan.TotalSeconds;           

        }

        static string FindStringWithHighestNumber(string[] stringArray)
        {
            // Using LINQ and custom comparison to find the string with the highest number
            string highestNumberString = stringArray
                .OrderByDescending(str => ExtractNumberFromString(str))
                .FirstOrDefault();

            return highestNumberString;
        }

        static int ExtractNumberFromString(string input)
        {
            // Extract the number from the input string
            string numberString = new string(input.Where(char.IsDigit).ToArray());

            if (int.TryParse(numberString, out int number))
            {
                return number;
            }

            // If there's no number in the input string, return a default value (e.g., 0)
            return 0;
        }

        static void ClearCurrentConsoleLine()
        {
            int currentLineCursor = Console.CursorTop;
            Console.SetCursorPosition(0, Console.CursorTop);
            Console.Write(new string(' ', Console.WindowWidth));
            Console.SetCursorPosition(0, currentLineCursor);
        }

        [DllImport("user32.dll")]
        
        public static extern IntPtr FindWindow(string lpClassName, string lpWindowName);
        
        [DllImport("user32.dll")]
        
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);
        
        public static void SetConsoleWindowVisibility(bool visible)
        {
            IntPtr hWnd = FindWindow(null, Console.Title);
            if (hWnd != IntPtr.Zero)
            {
                if (visible) ShowWindow(hWnd, 1); //1 = SW_SHOWNORMAL                
                else ShowWindow(hWnd, 0); //0 = SW_HIDE               
            }
        }

        public static void tray_DoubleClick(object sender, EventArgs e)
        {
            Visible = true;
            SetConsoleWindowVisibility(Visible);
        }

        public static void tray_MouseLeave(object sender, EventArgs e)
        {
            contextMenu.Hide();
        }

        public static void tray_Click(object sender, EventArgs e)
        {
            tray.ContextMenuStrip.Show(Cursor.Position);
            tray.ContextMenuStrip.BringToFront();
        }

        public static void trayMenuItem0_MouseHover(object sender, EventArgs e)
        { contextMenu.Items[0].ForeColor = Color.Black; }

        public static void trayMenuItem0_MouseLeave(object sender, EventArgs e)
        { contextMenu.Items[0].ForeColor = Color.White; }

        public static void trayMenuItem1_MouseHover(object sender, EventArgs e)
        { contextMenu.Items[1].ForeColor = Color.Black; }

        public static void trayMenuItem1_MouseLeave(object sender, EventArgs e)
        { contextMenu.Items[1].ForeColor = Color.White; }

        public static void trayMenuItem2_MouseHover(object sender, EventArgs e)
        { contextMenu.Items[2].ForeColor = Color.Black; }

        public static void trayMenuItem2_MouseLeave(object sender, EventArgs e)
        { contextMenu.Items[2].ForeColor = Color.White; }

        #region To Do`s       

        //TODO: Resume
        //TODO: Last menu position
        //TODO: send playlist 
        //TODO: Displaychanger & mpc timming
        //TODO: Implementation of batch files
        //TODO: Implementation of remote control
        //TODO: Playerrecognition
        //TODO: Playercoreconfig generation
        //TODO: Versioning
        //TODO: Setup        

        #endregion

    }
}
